from unicodedata import category
from django.contrib import admin
from django.urls import path

from blog.views import *
from shop.views import *

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', homepage, name='homepage'),
    path('blog/', blogpage, name='frontpage'),
    path('blog/post/<int:post_id>/', show_post, name='post'),
    path('admin/', admin.site.urls),
    path('catalog/', catalog, name='catalog'),
    path('catalog/category/<int:category_id>', categoies),
    path('catalog/brand/<int:brand_id>', brands),
    path('catalog/product/<int:product_id>', show_product, name='product'),
]

urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)