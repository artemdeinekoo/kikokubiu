from django.http import HttpResponse
from django.shortcuts import render
from .models import *


def blogpage(request):
    posts = Post.objects.all()

    return render(request, 'blog/blogpage.html', {'posts': posts})

def show_post(request, post_id):
    specificPost = Post.get_by_id(post_id)
    return render(request, 'blog/article.html', {'specificPost': specificPost})

