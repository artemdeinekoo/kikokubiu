from django.db import models
from requests import post

class Post(models.Model):
    title = models.CharField(max_length=255)
    intro = models.TextField()
    image = models.ImageField(null = True, blank = True, upload_to="images/")
    body = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-date_added']

    def __str__(self):
        return self.title

    @staticmethod
    def get_by_id(post_id):
        try:
            blogPost = Post.objects.get(id=post_id)
            return blogPost
        except Post.DoesNotExist:
            pass
