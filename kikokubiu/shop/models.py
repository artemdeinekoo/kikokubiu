from unicodedata import category, name
from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_all_products_by_category(category_id):
        return Product.objects.filter(category_id)

class Brand(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_all_products_by_brand(brand_id):
        return Product.objects.filter(brand_id)



class Product(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    category_id = models.ManyToManyField(Category, related_name='products')
    characteristic = models.TextField()
    brand_id = models.ManyToManyField(Brand, related_name='products')
    price = models.CharField(max_length=255)
    image = models.ImageField(null = True, blank = True, upload_to="images/")

    def __str__(self):
        return self.name

    @staticmethod
    def get_by_id(product_id):
        try:
            product = Product.objects.get(id=product_id)
            return product
        except Product.DoesNotExist:
            pass
