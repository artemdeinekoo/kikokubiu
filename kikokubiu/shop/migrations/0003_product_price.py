# Generated by Django 3.1.1 on 2022-06-21 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_auto_20220621_1352'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='price',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
    ]
