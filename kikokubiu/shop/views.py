from django.shortcuts import render
from .models import Product, Category, Brand


def homepage(request):
    products = Product.objects.all()

    return render(request, 'shop/homepage.html', {'products': products})

def catalog(request):
    return render(request, 'shop/catalog.html')

def categoies(request, category_id):
    products = Product.objects.filter(category_id = category_id)
    
    return render(request, 'shop/categorie_products.html', {'products':  products})

def brands(request, brand_id):
    products = Product.objects.filter(brand_id = brand_id)
    return render(request, 'shop/categorie_products.html', {'products':  products})

def show_product(request, product_id):
    specificProduct = Product.get_by_id(product_id)
    return render(request, 'shop/product.html', {'specificProduct': specificProduct})